package com.lf.museumguide.model;

public class Museum implements Items {

    public String id;
    public String name;
    public String monOpen, monClose;
    public String tueOpen, tueClose;
    public String wedOpen, wedClose;
    public String thuOpen, thuClose;
    public String friOpen, friClose;
    public String sabOpen, sabClose;
    public String sunOpen, sunClose;
    public boolean bMon, bTue, bThu, bWed, bFri, bSab, bSun;
    public String location;
    public double lat;
    public double lon;
    public double distance;

    public Museum(){}

    public Museum(
            String id,
            String name,
            boolean bMon,
            String monOpen,
            String monClose,
            boolean bTue,
            String tueOpen,
            String tueClose,
            boolean bWed,
            String wedOpen,
            String wedClose,
            boolean bThu,
            String thuOpen,
            String thuClose,
            boolean bFri,
            String friOpen,
            String friClose,
            boolean bSab,
            String sabOpen,
            String sabClose,
            boolean bSun,
            String sunOpen,
            String sunClose,
            String location,
            double lat,
            double lon
    ){

        this.id = id;
        this.name = name;

        this.bMon = bMon;
        this.monOpen = monOpen;
        this.monClose = monClose;
        this.bTue = bTue;
        this.tueOpen = tueOpen;
        this.tueClose = tueClose;
        this.bWed = bWed;
        this.wedOpen = wedOpen;
        this.wedClose = wedClose;
        this.bThu = bThu;
        this.thuOpen = thuOpen;
        this.thuClose = thuClose;
        this.bFri = bFri;
        this.friOpen = friOpen;
        this.friClose = friClose;
        this.bSab = bSab;
        this.sabOpen = sabOpen;
        this.sabClose = sabClose;
        this.bSun = bSun;
        this.sunOpen = sunOpen;
        this.sunClose = sunClose;

        this.location = location;
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }
}
