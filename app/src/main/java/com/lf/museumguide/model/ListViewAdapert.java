package com.lf.museumguide.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lf.museumguide.R;

import java.util.List;

public class ListViewAdapert extends ArrayAdapter<Items> {

    private int resourceLayout;
    private Context mContext;
    private int resId;

    public ListViewAdapert(Context context, int resource, List<Items> items, int resId) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
        this.resId = resId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        Items i = getItem(position);

        if (i != null) {
            TextView txtName = v.findViewById(R.id.txtName);
            ImageView imgName = v.findViewById(R.id.imgName);

            txtName.setText(i.getName());

            imgName.setImageResource(resId);

        }

        return v;
    }

}