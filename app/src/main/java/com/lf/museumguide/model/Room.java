package com.lf.museumguide.model;

public class Room implements Items {

    public String id;
    public String id_museum;
    public String name;

    public Room(){}

    public Room(String id, String id_museum, String name){
        this.id = id;
        this.id_museum = id_museum;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }
}
