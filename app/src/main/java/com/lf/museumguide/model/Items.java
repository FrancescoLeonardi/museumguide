package com.lf.museumguide.model;

public interface Items {
    public String getName();
    public String getId();
}
