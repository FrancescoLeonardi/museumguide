package com.lf.museumguide.model;

public class ObjectMusuem implements Items {

    public String id;
    public String id_room;
    public String name;
    public String image;
    public String description;

    public ObjectMusuem(){}

    public ObjectMusuem(String id, String id_room, String name, String image, String description){
        this.id = id;
        this.id_room = id_room;
        this.name = name;
        this.image = image;
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }
}
