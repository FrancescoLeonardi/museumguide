package com.lf.museumguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.museumguide.model.Museum;
import com.lf.museumguide.model.Room;

public class ViewMuseum extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnModify, btnDelete, btnRoom, btnObject;
    Button btnModifyRoom, btnDeleteRoom, btnModifyObject, btnDeleteObject;
    TextView title, txtCity, txtMonday, txtTuesday, txtWednesday, txtThursday, txtFriday, txtSaturday, txtSunday;

    String museumId;
    Museum m;

    Intent toListView, toModify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_museum);

        toListView = new Intent(this, MainActivity.class);
        toModify = new Intent(this, MuseumActivity.class);

        btnDelete = findViewById(R.id.btnDelete2);
        btnRoom = findViewById(R.id.btnRoom2);
        btnObject = findViewById(R.id.btnObject2);
        btnModify = findViewById(R.id.btnModify2);

        btnModifyRoom = findViewById(R.id.btnModifyRoom);
        btnDeleteRoom = findViewById(R.id.btnDeleteRoom);
        btnModifyObject = findViewById(R.id.btnModifyObject);
        btnDeleteObject = findViewById(R.id.btnDeleteObject);

        progressBar = findViewById(R.id.progressBar);

        title = findViewById(R.id.txtName);
        txtCity = findViewById(R.id.txtLocation);
        txtMonday = findViewById(R.id.mon_openclose);
        txtTuesday = findViewById(R.id.tue_openclose);
        txtWednesday = findViewById(R.id.wed_openclose);
        txtThursday = findViewById(R.id.thu_openclose);
        txtFriday = findViewById(R.id.fri_openclose);
        txtSaturday = findViewById(R.id.sat_openclose);
        txtSunday = findViewById(R.id.sun_openclose);

        museumId = getIntent().getStringExtra(Constant.MUSEUM);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final FirebaseFirestore db = FirebaseFirestore.getInstance();
                if(m != null){
                    progressBar.setVisibility(View.VISIBLE);
                        db.collection(Constant.ROOM)
                                .whereEqualTo("id_museum", m.id)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful()) {
                                            for (QueryDocumentSnapshot document : task.getResult()) {

                                                Room r = document.toObject(Room.class);

                                                db.collection(Constant.OBJECT)
                                                        .whereEqualTo("id_room", r.id)
                                                        .get()
                                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                if (task.isSuccessful()) {
                                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                                        Constant.deleteObject(document.getId());
                                                                    }
                                                                }
                                                            }
                                                        });
                                                Constant.deleteRooms(r.id);
                                            }
                                            Constant.deleteMuseum(m.id);
                                            finish();
                                        }
                                    }
                                });

                }
            }
        });

        btnRoom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toListView.putExtra(Constant.MODE, Constant.IROOM);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);
            }
        });

        btnObject.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toListView.putExtra(Constant.MODE, Constant.IOBJECT_FROM_MUSEUM);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);

            }
        });

        btnModify.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toModify.putExtra(Constant.ID, m.id);
                startActivity(toModify);
            }
        });

        btnModifyRoom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toListView.putExtra(Constant.MODE, Constant.IMODIFY_ROOM);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);

            }
        });

        btnDeleteRoom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toListView.putExtra(Constant.MODE, Constant.IDELETE_ROOM);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);

            }
        });

        btnModifyObject.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toListView.putExtra(Constant.MODE, Constant.IMODIFY_OBJECT);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);

            }
        });

        btnDeleteObject.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                toListView.putExtra(Constant.MODE, Constant.IDELETE_OBJECT);
                toListView.putExtra(Constant.ID_MODE, m.id);
                startActivity(toListView);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

       ViewMuseum.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadMuseum();
            }
        });
    }

    public void loadMuseum(){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        progressBar.setVisibility(View.VISIBLE);
        db.collection(Constant.MUSEUM)
                .whereEqualTo("id", museumId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                m = document.toObject(Museum.class);

                                title.setText(m.name);
                                if(m.location.isEmpty())
                                    txtCity.setText("Lat: "+m.lat+ ", Lon: "+m.lon);
                                else
                                    txtCity.setText(m.location);

                                txtMonday.setText(patternTime(m.bMon, Constant.adjuctTimeView(m.monOpen), Constant.adjuctTimeView(m.monClose)));
                                txtTuesday.setText(patternTime(m.bTue, Constant.adjuctTimeView(m.tueOpen), Constant.adjuctTimeView(m.tueClose)));
                                txtWednesday.setText(patternTime(m.bWed, Constant.adjuctTimeView(m.wedOpen), Constant.adjuctTimeView(m.wedClose)));
                                txtThursday.setText(patternTime(m.bThu, Constant.adjuctTimeView(m.thuOpen), Constant.adjuctTimeView(m.thuClose)));
                                txtFriday.setText(patternTime(m.bFri, Constant.adjuctTimeView(m.friOpen), Constant.adjuctTimeView(m.friClose)));
                                txtSaturday.setText(patternTime(m.bSab, Constant.adjuctTimeView(m.sabOpen), Constant.adjuctTimeView(m.sabClose)));
                                txtSunday.setText(patternTime(m.bSun, Constant.adjuctTimeView(m.sunOpen), Constant.adjuctTimeView(m.sunClose)));

                            }

                        } else {
                            Constant.showMessageOK(ViewMuseum.this, "Error getting document");
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    private String patternTime(boolean set, String timeOpen, String timeClose){
        if(set)
            return timeOpen + " - "+timeClose;
        else
            return "Close";
    }


}
