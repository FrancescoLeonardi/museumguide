package com.lf.museumguide;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.lf.museumguide.model.Items;
import com.lf.museumguide.model.Museum;
import com.lf.museumguide.model.ObjectMusuem;
import com.lf.museumguide.model.Room;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Constant {

    public final static String TAG = "MuseumAPP";
    public final static String MUSEUM = "museum";
    public final static String ROOM = "room";
    public final static String OBJECT = "object";
    public final static String ID = "ID";
    public final static String ID_S = "ID_S";

    public final static String MODE = "mode";
    public final static String ID_MODE = "id_mode";

    public final static int IMUSEUM = 0;
    public final static int IROOM = 1;
    public final static int IOBJECT = 2;
    public final static int IOBJECT_FROM_MUSEUM = 3;

    public final static int IDELETE_ROOM = 4;
    public final static int IMODIFY_ROOM = 5;
    public final static int IDELETE_OBJECT = 6;
    public final static int IMODIFY_OBJECT = 7;

    public static void deleteMuseum(String id_museum){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(Constant.ROOM)
                .whereEqualTo("id_museum", id_museum)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Room r = document.toObject(Room.class);
                                deleteRooms(r.getId());
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection(Constant.MUSEUM).document(id_museum)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Museum elimitated");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }

    public static void deleteRooms(String id_room){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(Constant.OBJECT)
                .whereEqualTo("id_room", id_room)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ObjectMusuem om = document.toObject(ObjectMusuem.class);
                                deleteObject(om.getId());
                            }

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection(Constant.ROOM).document(id_room)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Room elimitated");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }

    public static void deleteObject(String id_object){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        // Create a reference to the file to delete
        StorageReference imgRef = storageRef.child(id_object+".jpg");

        // Delete the file
        imgRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Uh-oh, an error occurred!
            }
        });

        db.collection(Constant.OBJECT).document(id_object)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w(TAG, "Object elimitated");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }


    public static void loadAllMuseum(final List<Items> items, final ListView lst, final ProgressBar progressBar, final Calendar calendar, final double lat, final double lon){

        items.clear();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Constant.MUSEUM)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            items.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Museum m = document.toObject(Museum.class);
                                Log.d(TAG,m.getName());
                                Log.d(TAG, Boolean.toString( isOpenDay(m, calendar) ));
                                if(isOpenDay(m, calendar)) {
                                    distance(lat, lon, m.lat, m.lon);
                                    Log.d(TAG, Double.toString( distance(lat, lon, m.lat, m.lon) ));
                                    items.add(m);
                                }
                            }

                            //ordino array
                            // Sorting
                            Collections.sort(items, new Comparator<Items>() {
                                @Override
                                public int compare(Items c1, Items c2) {
                                    return Double.compare(((Museum)c1).distance, ((Museum)c2).distance);
                                }
                            });


                            ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                        } else {
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    public static void loadRoom(final String idMuseum, final List<Items> items,final ListView lst,final ProgressBar progressBar){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        items.clear();

        db.collection(Constant.ROOM)
                .whereEqualTo("id_museum", idMuseum)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            items.clear();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Room r = document.toObject(Room.class);
                                items.add(r);
                            }

                            ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    public static void loadObject(final String idRoom, final List<Items> items,final ListView lst,final ProgressBar progressBar){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        items.clear();

        db.collection(Constant.OBJECT)
            .whereEqualTo("id_room", idRoom)
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        items.clear();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            ObjectMusuem om = document.toObject(ObjectMusuem.class);
                            items.add(om);
                            }
                        ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();

                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }

                    progressBar.setVisibility(View.GONE);
                }
            });
    }

    public static void loadObjectFromMuseum(final String idMuseum, final List<Items> items,final ListView lst,final ProgressBar progressBar){

        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        items.clear();

        db.collection(Constant.ROOM)
                .whereEqualTo("id_museum", idMuseum)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                db.collection(Constant.OBJECT)
                                        .whereEqualTo("id_room", document.getId())
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {

                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        ObjectMusuem om = document.toObject(ObjectMusuem.class);
                                                        items.add(om);
                                                    }
                                                    ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                                                    progressBar.setVisibility(View.GONE);

                                                } else {
                                                    Log.d(TAG, "Error getting documents: ", task.getException());
                                                    progressBar.setVisibility(View.GONE);
                                                }

                                            }
                                        });

                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });

    }


    public static void showMessageOK(Context con, String message) {
        new AlertDialog.Builder(con)
                .setMessage(message)
                .setNegativeButton("Ok", null)
                .create()
                .show();
    }

    public static ArrayList<Items> searchMuseum(ArrayList<Items> items, String text_search) {
        ArrayList<Items> tmp = new ArrayList<>();
        String search = text_search.toLowerCase();
        for(Items i : items){

            Museum m = ((Museum) i);

            if(m.name.toLowerCase().contains(search))
                tmp.add(m);

        }

        return tmp;
    }

    public static ArrayList<Items> searchRoom(ArrayList<Items> items, String text_search) {
        ArrayList<Items> tmp = new ArrayList<>();
        String search = text_search.toLowerCase();
        for(Items i : items){

            Room r = ((Room) i);

            if(r.name.toLowerCase().contains(search))
                tmp.add(r);

        }

        return tmp;
    }

    public static ArrayList<Items> searchObject(ArrayList<Items> items, String text_search) {
        ArrayList<Items> tmp = new ArrayList<>();
        String search = text_search.toLowerCase();
        for(Items i : items){

            ObjectMusuem o = ((ObjectMusuem) i);

            if(o.name.toLowerCase().contains(search))
                tmp.add(o);

        }

        return tmp;
    }

    public static void showClock(Context con, final TextView text){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(con, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                text.setText( adjuctTimeView(selectedHour + ":" + selectedMinute));
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private static boolean isOpenDay(Museum m, Calendar calendar){

        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                if(m.bSun)
                    return isInTime(calendar, m.sunOpen, m.sunClose);
                else
                    return false;

            case Calendar.MONDAY:
                if(m.bMon)
                    return isInTime(calendar, m.monOpen, m.monClose);
                else
                    return false;

            case Calendar.TUESDAY:
                if(m.bTue)
                    return isInTime(calendar, m.tueOpen, m.tueClose);
                else
                    return false;

            case Calendar.WEDNESDAY:
                if(m.bWed)
                    return isInTime(calendar, m.wedOpen, m.wedClose);
                else
                    return false;

            case Calendar.THURSDAY:
                if(m.bThu)
                    return isInTime(calendar, m.thuOpen, m.thuClose);
                else
                    return false;

            case Calendar.FRIDAY:
                if(m.bFri)
                    return isInTime(calendar, m.friOpen, m.friClose);
                else
                    return false;

            case Calendar.SATURDAY:
                if(m.bSab)
                    return isInTime(calendar, m.sabOpen, m.sabClose);
                else
                    return false;
        }
        return true;
    }

    private static boolean isInTime(Calendar c, String open, String close){

        try{
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            String[] split_open = open.split(":");
            String[] split_close = close.split(":");

            int hour_open = Integer.parseInt(split_open[0]);
            int hour_close = Integer.parseInt(split_close[0]);

            int min_open = Integer.parseInt(split_open[1]);
            int min_close = Integer.parseInt(split_open[1]);

            if(hour > hour_open && hour < hour_close)
                return true;

            if(hour == hour_open) {
                if (minute >= min_open)
                    return true;
            }

            if(hour == hour_close){
                if(minute <= min_close)
                    return true;
            }

            return false;
        }catch (Exception ex){
            return false;
        }

    }

    private static double distance(Double lat1, Double lon1, double lat2, double lon2){
        if(lat1 != null && lon1 !=null){

            final int R = 6371; // Radius of the earth

            double latDistance = Math.toRadians(lat2 - lat1);
            double lonDistance = Math.toRadians(lon2 - lon1);
            double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                    + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                    * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c * 1000; // convert to meters

            distance = Math.pow(distance, 2);

            return Math.sqrt(distance);
        }
        return 0;
    }

    public static String adjuctTimeView(String time){
        try{
            if(time.length() == 5)
                return time;
            String return_string;
            String[] split_time = time.split(":");

            if(split_time[0].length() < 2)
                return_string = "0"+split_time[0];
            else
                return_string = split_time[0];

            return_string += ":";

            if(split_time[1].length() < 2)
                return_string += "0"+split_time[1];
            else
                return_string += split_time[1];

            return return_string;

        }catch(Exception ex){
            return time;
        }
    }

    public static void downloadImage(final Context con, final ImageView imageView, String img,final ProgressBar progressBar){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        // Points to the root reference
        storageRef.child(img).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                Picasso.with(con).load(uri).into(imageView);
                progressBar.setVisibility(View.GONE);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                showMessageOK(con, "Error download image");
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    public static void uploadImage(final Context con, ImageView imageView, String img){

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference mountainsRef = storageRef.child(img);

        // Get the data from an ImageView as bytes
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = mountainsRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                showMessageOK(con, "Error load image");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

            }
        });
    }

}
