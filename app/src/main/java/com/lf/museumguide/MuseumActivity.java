package com.lf.museumguide;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.museumguide.model.Museum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MuseumActivity extends AppCompatActivity {

    //TODO: controllare il fatto del microfono delle autorizzazioni
    ProgressBar progressBar;
    Button btnSend, btnHere;
    EditText edtxtName, edtxtLocation, edtxtLat, edtxtLon;
    CheckBox cbMon, cbTue, cbWed, cbThu, cbFri, cbSat, cbSun;
    TextView txtMonOpen, txtTueOpen, txtWedOpen, txtThuOpen, txtFriOpen, txtSabOpen, txtSunOpen;
    TextView txtMonClose, txtTueClose, txtWedClose, txtThuClose, txtFriClose, txtSabClose, txtSunClose;

    private FirebaseFirestore db;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    private String id;
    private boolean yet = true;

    private Museum m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_museum);

        UIload();

        id = getIntent().getStringExtra(Constant.ID);
        if(id == null) {
            id = "";
            yet = false;
        }

        db = FirebaseFirestore.getInstance();

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);

        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


        MuseumActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                checkExits();
            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkField()) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (yet){
                        Museum m_new = new Museum(
                                m.id,
                                edtxtName.getText().toString(),

                                cbMon.isChecked(),
                                txtMonOpen.getText().toString(),
                                txtMonClose.getText().toString(),

                                cbTue.isChecked(),
                                txtTueOpen.getText().toString(),
                                txtTueClose.getText().toString(),

                                cbWed.isChecked(),
                                txtWedOpen.getText().toString(),
                                txtWedClose.getText().toString(),

                                cbThu.isChecked(),
                                txtThuOpen.getText().toString(),
                                txtThuClose.getText().toString(),

                                cbFri.isChecked(),
                                txtFriOpen.getText().toString(),
                                txtFriClose.getText().toString(),

                                cbSat.isChecked(),
                                txtSabOpen.getText().toString(),
                                txtSabClose.getText().toString(),

                                cbSun.isChecked(),
                                txtSunOpen.getText().toString(),
                                txtSunClose.getText().toString(),

                                edtxtLocation.getText().toString(),
                                Double.parseDouble(edtxtLat.getText().toString()),
                                Double.parseDouble(edtxtLon.getText().toString())
                        );

                        db.collection(Constant.MUSEUM).document(m.id).set(m_new);
                        finish();
                    }
                    else{
                        DocumentReference ref = db.collection(Constant.MUSEUM).document();
                        String idTmp = ref.getId();

                        m = new Museum(
                                idTmp,
                                edtxtName.getText().toString(),

                                cbMon.isChecked(),
                                txtMonOpen.getText().toString(),
                                txtMonClose.getText().toString(),

                                cbTue.isChecked(),
                                txtTueOpen.getText().toString(),
                                txtTueClose.getText().toString(),

                                cbWed.isChecked(),
                                txtWedOpen.getText().toString(),
                                txtWedClose.getText().toString(),

                                cbThu.isChecked(),
                                txtThuOpen.getText().toString(),
                                txtThuClose.getText().toString(),

                                cbFri.isChecked(),
                                txtFriOpen.getText().toString(),
                                txtFriClose.getText().toString(),

                                cbSat.isChecked(),
                                txtSabOpen.getText().toString(),
                                txtSabClose.getText().toString(),

                                cbSun.isChecked(),
                                txtSunOpen.getText().toString(),
                                txtSunClose.getText().toString(),

                                edtxtLocation.getText().toString(),
                                Double.parseDouble(edtxtLat.getText().toString()),
                                Double.parseDouble(edtxtLon.getText().toString())
                        );

                        db.collection(Constant.MUSEUM).document(idTmp).set(m);
                        yet = true;
                        finish();
                    }
                }
            }
        });

        btnHere.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try{
                    Geocoder coder = new Geocoder(MuseumActivity.this, Locale.getDefault());
                    List<Address> address = coder.getFromLocationName(edtxtLocation.getText().toString(), 1);

                    if(address != null) {
                        edtxtLat.setText(Double.toString( address.get(0).getLatitude()) );
                        edtxtLon.setText(Double.toString( address.get(0).getLongitude()));
                    }
                    else{
                        edtxtLat.setText(0);
                        edtxtLon.setText(0);
                    }
                }catch(IOException ex){
                    Constant.showMessageOK(MuseumActivity.this, "Location not found, put your coordinates");
                }

            }
        });
    }


    private void UIload(){

        edtxtName = findViewById(R.id.edtxtName);
        edtxtLocation = findViewById(R.id.edtxtLocation);
        edtxtLat = findViewById(R.id.edtxtLat);
        edtxtLon = findViewById(R.id.edtxtLon);

        btnSend = findViewById(R.id.btnSend);
        btnHere = findViewById(R.id.btnHere);

        progressBar = findViewById(R.id.progressBar);

        cbMon = findViewById(R.id.cbMon);
        cbTue = findViewById(R.id.cbTue);
        cbWed = findViewById(R.id.cbWed);
        cbThu = findViewById(R.id.cbThu);
        cbFri = findViewById(R.id.cbFri);
        cbSat = findViewById(R.id.cbSat);
        cbSun = findViewById(R.id.cbSun);

        txtMonOpen = findViewById(R.id.txtMonOpen);
        txtTueOpen = findViewById(R.id.txtTueOpen);
        txtWedOpen = findViewById(R.id.txtWedOpen);
        txtThuOpen = findViewById(R.id.txtThuOpen);
        txtFriOpen = findViewById(R.id.txtFriOpen);
        txtSabOpen = findViewById(R.id.txtSabOpen);
        txtSunOpen = findViewById(R.id.txtSunOpen);

        txtMonClose = findViewById(R.id.txtMonClose);
        txtTueClose = findViewById(R.id.txtTueClose);
        txtWedClose = findViewById(R.id.txtWedClose);
        txtThuClose = findViewById(R.id.txtThuClose);
        txtFriClose = findViewById(R.id.txtFriClose);
        txtSabClose = findViewById(R.id.txtSabClose);
        txtSunClose = findViewById(R.id.txtSunClose);
        btnManager();
    }

    private void btnManager(){

        txtMonOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtMonOpen);
            }
        });

        txtTueOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtTueOpen);
            }
        });

        txtWedOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtWedOpen);
            }
        });

        txtThuOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtThuOpen);
            }
        });

        txtFriOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtFriOpen);
            }
        });

        txtSabOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtSabOpen);
            }
        });

        txtSunOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtSunOpen);
            }
        });

        txtMonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtMonClose);
            }
        });

        txtTueClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtTueClose);
            }
        });

        txtWedClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtWedClose);
            }
        });

        txtThuClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtThuClose);
            }
        });

        txtFriClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtFriClose);
            }
        });

        txtSabClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtSabClose);
            }
        });

        txtSunClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.showClock(MuseumActivity.this, txtSunClose);
            }
        });

    }


    private void checkExits(){

        db.collection(Constant.MUSEUM)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                yet = true;
                                m = document.toObject(Museum.class);
                                afterLoad();
                            }
                        } else {
                            Log.d(Constant.TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        progressBar.setVisibility(View.GONE);

    }

    private void afterLoad(){
        Log.d(Constant.TAG, "Found Trip: " + m.id + " => " + m.name);
        edtxtName.setText(m.name);

        cbMon.setChecked(m.bMon);
        txtMonOpen.setText(m.monOpen);
        txtMonClose.setText(m.monClose);

        cbTue.setChecked(m.bTue);
        txtTueOpen.setText(m.tueOpen);
        txtTueClose.setText(m.tueClose);

        cbWed.setChecked(m.bWed);
        txtWedOpen.setText(m.wedOpen);
        txtWedClose.setText(m.wedClose);

        cbThu.setChecked(m.bThu);
        txtThuOpen.setText(m.thuOpen);
        txtThuClose.setText(m.thuClose);

        cbFri.setChecked(m.bFri);
        txtFriOpen.setText(m.friOpen);
        txtFriClose.setText(m.friClose);

        cbSat.setChecked(m.bSab);
        txtSabOpen.setText(m.sabOpen);
        txtSabClose.setText(m.sabClose);

        cbSun.setChecked(m.bSun);
        txtSunOpen.setText(m.sunOpen);
        txtSunClose.setText(m.sunClose);

        edtxtLocation.setText(m.location);
        edtxtLat.setText(Double.toString(m.lat));
        edtxtLon.setText(Double.toString(m.lon));
        progressBar.setVisibility(View.GONE);
    }

    private boolean checkField(){
        if(edtxtName.getText().toString().isEmpty()){
            showMessageOK("Name is empty");
            return false;
        }

        if(edtxtLat.getText().toString().isEmpty()){
            showMessageOK("Latitude is empty");
            return false;
        }

        if(edtxtLon.getText().toString().isEmpty()){
            showMessageOK("Longitude is empty");
            return false;
        }


        return true;
    }

    //region gps

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }


    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MuseumActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void showMessageOK(String message) {
        new AlertDialog.Builder(MuseumActivity.this)
                .setMessage(message)
                .setNegativeButton("Ok", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  locationTrack.stopListener();
    }

    //endregion
}
