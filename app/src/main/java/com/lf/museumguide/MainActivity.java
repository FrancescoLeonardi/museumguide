package com.lf.museumguide;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lf.museumguide.model.*;

import java.util.ArrayList;
import java.util.Calendar;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.RECORD_AUDIO;

public class MainActivity extends AppCompatActivity{

    double lat, lon;

    ArrayList<Items> items;
    ImageView btnPlus;
    Button btnSearch;
    EditText edtxtSearch;
    ImageView imgLogo;
    ListView lst;
    TextView txtGps, title;
    ProgressBar progressBar;

    Intent toEdit, toView;

    int mode;
    String idIntent;


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTracker tracker;

    String sGps;
    boolean bGps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        try{

            items = new ArrayList<>();

            toEdit = new Intent(this, MuseumActivity.class);
            toView = new Intent(this, ViewMuseum.class);

            idIntent = getIntent().getStringExtra(Constant.ID_MODE);                  // id della sotto tabella
            mode = getIntent().getIntExtra(Constant.MODE, Constant.IMUSEUM);    // mode

            imgLogo = findViewById(R.id.imgLogo);
            lst = findViewById(R.id.listView);
            btnPlus = findViewById(R.id.btnPlus);
            btnSearch = findViewById(R.id.btnSearch);
            edtxtSearch = findViewById(R.id.edtxtSearch);
            txtGps = findViewById(R.id.txtGps);
            title = findViewById(R.id.txtTitle);
            progressBar = findViewById(R.id.progressBar);

            bGps = false;

            permissions.add(ACCESS_FINE_LOCATION);
            permissions.add(ACCESS_COARSE_LOCATION);
            permissions.add(RECORD_AUDIO);

            permissionsToRequest = findUnAskedPermissions(permissions);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


                if (permissionsToRequest.size() > 0)
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }

            sGps = (String) android.text.format.DateFormat.format("HH:mm", new java.util.Date());
            txtGps.setText(sGps);

            loadMode();

            btnPlus.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    startActivity(toEdit);
                }
            });

            //region search
            btnSearch.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    progressBar.setVisibility(View.VISIBLE);
                    ListViewAdapert customAdapter = null;
                    String text_search = edtxtSearch.getText().toString();

                    if(text_search.isEmpty())
                    {
                        switch(mode){

                            case Constant.IMUSEUM:
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, items, R.drawable.museum);
                                break;

                            case Constant.IROOM:
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, items, R.drawable.room);
                                break;

                            case Constant.IOBJECT_FROM_MUSEUM:
                            case Constant.IOBJECT:
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, items, R.drawable.bust);
                                break;

                        }
                    }
                    else
                    {
                        ArrayList<Items> tmp_item = null;
                        switch(mode){

                            case Constant.IMUSEUM:
                                tmp_item = Constant.searchMuseum(items, text_search);
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, tmp_item, R.drawable.museum);
                                break;

                            case Constant.IROOM:
                                tmp_item = Constant.searchRoom(items, text_search);
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, tmp_item, R.drawable.room);
                                break;

                            case Constant.IOBJECT_FROM_MUSEUM:
                            case Constant.IOBJECT:
                                tmp_item = Constant.searchObject(items, text_search);
                                customAdapter = new ListViewAdapert(MainActivity.this, R.layout.row_listview, tmp_item, R.drawable.bust);
                                break;

                        }


                    }

                    lst.setAdapter(customAdapter);
                    ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);

                }
            });
            //endregion

        }catch(Exception ex){
            Constant.showMessageOK(MainActivity.this, "Something went wrong!");
        }

    }


    private void loadMode(){

        if(progressBar.getVisibility()!= View.VISIBLE)
            progressBar.setVisibility(View.VISIBLE);

        switch(mode){

            case Constant.IMUSEUM:
                imgLogo.setImageResource(R.drawable.museum);
                title.setText(R.string.museum);
                toEdit = new Intent(this, MuseumActivity.class);
                toView = new Intent(this, ViewMuseum.class);
                btnPlus.setVisibility(View.VISIBLE);
                break;

            case Constant.IROOM:
                imgLogo.setImageResource(R.drawable.room);
                title.setText(R.string.room);
                toEdit = new Intent(this, RoomActivity.class);
                toEdit.putExtra(Constant.ID_S, idIntent);
                toView = new Intent(this, MainActivity.class);
                btnPlus.setVisibility(View.VISIBLE);
                break;

            case Constant.IOBJECT:
                imgLogo.setImageResource(R.drawable.bust);
                title.setText(R.string.object);
                toEdit = new Intent(this, ObjectActivity.class);
                toEdit.putExtra(Constant.ID_S, idIntent);
                toView = new Intent(this, ViewActivity.class);
                btnPlus.setVisibility(View.VISIBLE);
                break;

            case Constant.IOBJECT_FROM_MUSEUM:
                imgLogo.setImageResource(R.drawable.bust);
                title.setText(R.string.object);
                toEdit = new Intent(this, ObjectActivity.class);
                toView = new Intent(this, ViewActivity.class);
                btnPlus.setVisibility(View.INVISIBLE);
                break;

            case Constant.IDELETE_ROOM:
                imgLogo.setImageResource(R.drawable.room);
                title.setText(R.string.delete_room);
                btnPlus.setVisibility(View.INVISIBLE);
                break;

            case Constant.IMODIFY_ROOM:
                imgLogo.setImageResource(R.drawable.room);
                title.setText(R.string.modify_room);
                toEdit = new Intent(this, RoomActivity.class);
                toEdit.putExtra(Constant.ID_S, idIntent);
                btnPlus.setVisibility(View.INVISIBLE);
                break;

            case Constant.IDELETE_OBJECT:
                imgLogo.setImageResource(R.drawable.bust);
                title.setText(R.string.delete_object);
                btnPlus.setVisibility(View.INVISIBLE);
                break;

            case Constant.IMODIFY_OBJECT:
                imgLogo.setImageResource(R.drawable.bust);
                title.setText(R.string.modify_object);
                toEdit = new Intent(this, ObjectActivity.class);
                toEdit.putExtra(Constant.ID_S, idIntent);
                btnPlus.setVisibility(View.INVISIBLE);
                break;


        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //region notPASS
        // get data from the table by the ListAdapter
        ListViewAdapert customAdapter = new ListViewAdapert(this, R.layout.row_listview, items, R.drawable.museum);
        lst.setAdapter(customAdapter);

        if(mode==Constant.IMUSEUM){
            tracker=new LocationTracker(MainActivity.this);
            // check if location is available
            if(tracker.isLocationEnabled)
            {
                lat = tracker.getLatitude();
                lon = tracker.getLongitude();

                bGps = true;

                if(lat==0 && lon == 0){
                    lat = tracker.getLatitude();
                    lon = tracker.getLongitude();
                }
            }
            else
            {
                // show dialog box to user to enable location
                try {
                    tracker.askToOnLocation();
                }
                catch(Exception ex){
                    Constant.showMessageOK(MainActivity.this, "Error with GPS");
                }
            }
        }

        //endregion

        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d(Constant.TAG,"Posizione numero: " +position);

                switch(mode){

                    case Constant.IMUSEUM:
                        toView.putExtra(Constant.MUSEUM, items.get(position).getId());
                        startActivity(toView);  //ViewMuseum    Museum --> View Museum
                        break;

                    case Constant.IROOM:
                        toView.putExtra(Constant.ID_MODE, items.get(position).getId());
                        toView.putExtra(Constant.MODE, Constant.IOBJECT);
                        startActivity(toView);  //MainActivity      Room --> Object
                        break;

                    case Constant.IOBJECT:
                         toView.putExtra(Constant.ID, items.get(position).getId());
                         startActivity(toView);  //View Activity      Object --> Object View
                        break;

                    case Constant.IOBJECT_FROM_MUSEUM:
                        toView.putExtra(Constant.ID, items.get(position).getId());
                        startActivity(toView);  //View Activity      Object --> Object View
                        break;


                    case Constant.IDELETE_ROOM:
                        final int position_tmp = position;
                        showMessageDeleteCancel("Are you sure to delete " + items.get(position_tmp).getName()+"?",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Constant.deleteRooms(items.get(position_tmp).getId());
                                    }
                                });
                        finish();
                        break;

                    case Constant.IMODIFY_ROOM:
                        toEdit.putExtra(Constant.ID, items.get(position).getId());
                        startActivity(toEdit);
                        break;

                    case Constant.IDELETE_OBJECT:
                        final int position_tmp_object = position;
                        showMessageDeleteCancel("Are you sure to delete " + items.get(position_tmp_object).getName()+"?" ,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Constant.deleteObject(items.get(position_tmp_object).getId());
                                        finish();
                                    }
                                });
                        break;

                    case Constant.IMODIFY_OBJECT:
                        toEdit.putExtra(Constant.ID, items.get(position).getId());
                        startActivity(toEdit);
                        break;

                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        try{
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    switch(mode){

                        case Constant.IMUSEUM:
                            Calendar cal = Calendar.getInstance();

                            if(lat == 0 && lon == 0)
                                if(bGps)
                                    Constant.showMessageOK(MainActivity.this, "Lat and Lon is 0. Are you really in the middle of the ocean?");

                            Constant.loadAllMuseum(items, lst, progressBar, cal, lat, lon);
                            break;

                        case Constant.IROOM:
                        case Constant.IDELETE_ROOM:
                        case Constant.IMODIFY_ROOM:
                            Constant.loadRoom(idIntent, items, lst, progressBar);//id museum da ViewMuseum
                            break;

                        case Constant.IOBJECT:
                            Constant.loadObject(idIntent, items, lst, progressBar);//id room da MainActivity
                            break;

                        case Constant.IOBJECT_FROM_MUSEUM:
                        case Constant.IDELETE_OBJECT:
                        case Constant.IMODIFY_OBJECT:
                            Constant.loadObjectFromMuseum(idIntent, items, lst, progressBar);//id museum da ViewMuseum
                            break;

                    }
                }
            });
        }catch(IllegalStateException ex){
            Constant.showMessageOK(MainActivity.this, "Something went wrong!");
        }

    }


    //region gps

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }


    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private void showMessageDeleteCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("Delete", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  locationTrack.stopListener();
    }

    //endregion

}
