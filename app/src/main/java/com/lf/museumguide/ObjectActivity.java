package com.lf.museumguide;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.museumguide.model.ObjectMusuem;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

public class ObjectActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSend, btnImage;
    ImageView btnRecord;
    EditText edtxtName, edtxtDescription;
    ImageView imageView;

    private FirebaseFirestore db;

    private String id, id_s;
    private boolean yet = true;

    private ObjectMusuem om;

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private static final int RESULT_LOAD_IMAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object);

        edtxtName = findViewById(R.id.edtxtName);
        edtxtDescription = findViewById(R.id.edtxtDescription);
        btnRecord = findViewById(R.id.btnRecord);

        btnSend = findViewById(R.id.btnSend);
        btnRecord = findViewById(R.id.btnRecord);
        btnImage = findViewById(R.id.btnImage);

        imageView = findViewById(R.id.imageView2);

        progressBar = findViewById(R.id.progressBar);

        id = getIntent().getStringExtra(Constant.ID);
        id_s = getIntent().getStringExtra(Constant.ID_S);

        if(id == null) {
            id = "";
            yet = false;
        }

        db = FirebaseFirestore.getInstance();

        ObjectActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                checkExits();
            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkField()) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (yet){
                        db.collection(Constant.OBJECT).document(om.id).set(om);
                        finish();
                    }
                    else{
                        DocumentReference ref = db.collection(Constant.OBJECT).document();
                        String idTmp = ref.getId();
                        om = new ObjectMusuem(
                                idTmp,
                                id_s,
                                edtxtName.getText().toString(),
                                idTmp + ".jpg",
                                edtxtDescription.getText().toString()
                        );
                        Constant.uploadImage(ObjectActivity.this,imageView, om.image);
                        db.collection(Constant.OBJECT).document(idTmp).set(om);
                        yet = true;
                        finish();
                    }
                }
            }
        });

        btnImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        btnRecord.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startVoiceInput();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
     //   if(progressBar.getVisibility() != View.VISIBLE)
     //       progressBar.setVisibility(View.VISIBLE);
    }

    private void checkExits(){

        db.collection(Constant.OBJECT)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                progressBar.setVisibility(View.VISIBLE);
                                yet = true;
                                om = document.toObject(ObjectMusuem.class);
                                Log.d(Constant.TAG, "Found Trip: " + om.id + " => " + om.name);
                                edtxtName.setText(om.name);
                                edtxtDescription.setText(om.description);
                                Constant.downloadImage(ObjectActivity.this, imageView, om.image, progressBar);
                                progressBar.setVisibility(View.GONE);
                            }
                        } else {
                            Log.d(Constant.TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        progressBar.setVisibility(View.GONE);

    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case RESULT_LOAD_IMAGE:

                if(resultCode == RESULT_OK && null != data) {
                    try {
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        imageView.setImageBitmap(selectedImage);
                        imageView.setMaxHeight(imageView.getWidth()/2*3);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }

                }
                break;

            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    edtxtDescription.setText(result.get(0));
                }
                break;
            }

        }
    }

    private boolean checkField(){
        if(edtxtName.getText().toString().isEmpty()){
            showMessageOK("Name is empty");
            return false;
        }

        if(edtxtDescription.getText().toString().isEmpty()){
            showMessageOK("Description is empty");
            return false;
        }

        if(((BitmapDrawable)imageView.getDrawable()).getBitmap() == null){
            showMessageOK("No image select");
            return false;
        }

        return true;
    }

    private void showMessageOK(String message) {
        new AlertDialog.Builder(ObjectActivity.this)
                .setMessage(message)
                .setNegativeButton("Ok", null)
                .create()
                .show();
    }
}
