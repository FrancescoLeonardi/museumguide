package com.lf.museumguide;

import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.museumguide.model.ObjectMusuem;

import java.util.Locale;

public class ViewActivity extends AppCompatActivity implements TextToSpeech.OnInitListener  {


    //TODO: gestire la progress bar

    ProgressBar progressBar;
    ImageView btnListen;
    TextView txtName, txtDescription;
    ImageView imageView;

    private FirebaseFirestore db;

    private String id;

    private ObjectMusuem om;

    TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        txtName = findViewById(R.id.txtObjectTitle);
        txtDescription = findViewById(R.id.txtDescription);
        btnListen = findViewById(R.id.btnListen);
        imageView = findViewById(R.id.imageView2);
        progressBar = findViewById(R.id.progressBar);

        textToSpeech = new TextToSpeech(this, this);

        id = getIntent().getStringExtra(Constant.ID);

        db = FirebaseFirestore.getInstance();

        ViewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        });

        btnListen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                texttoSpeak();
            }
        });
    }

    @Override
    protected void onDestroy(){
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(progressBar.getVisibility() != View.VISIBLE)
            progressBar.setVisibility(View.VISIBLE);
    }

    private void loadData(){

        if(progressBar.getVisibility() != View.VISIBLE)
            progressBar.setVisibility(View.VISIBLE);

        db.collection(Constant.OBJECT)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                om = document.toObject(ObjectMusuem.class);
                                Log.d(Constant.TAG, "Found Trip: " + om.id + " => " + om.name);
                                txtName.setText(om.name);
                                txtDescription.setText(om.description);
                                Constant.downloadImage(ViewActivity.this, imageView, om.image, progressBar);
                                progressBar.setVisibility(View.GONE);
                            }
                        } else {
                            Log.d(Constant.TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = textToSpeech.setLanguage(Locale.US);
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("error", "This Language is not supported");
            } else {
                texttoSpeak();
            }
        } else {
            Log.e("error", "Failed to Initialize");
        }
    }


    private void texttoSpeak() {
        String text = txtDescription.getText().toString();
        if ("".equals(text)) {
            text = "No description";
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }
}
