package com.lf.museumguide;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.museumguide.model.Room;

public class RoomActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSend;
    EditText edtxtName;

    private FirebaseFirestore db;

    private String id, id_s;
    private boolean yet = true;

    private Room r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        edtxtName = findViewById(R.id.edtxtName);

        btnSend = findViewById(R.id.btnSend);

        progressBar = findViewById(R.id.progressBar);

        id = getIntent().getStringExtra(Constant.ID);
        id_s = getIntent().getStringExtra(Constant.ID_S);

        Log.d("ID_S", "ID-S: " + id_s );

        if(id == null) {
            id = "";
            yet = false;
        }

        db = FirebaseFirestore.getInstance();

        new Thread(new Runnable() {
            @Override
            public void run()
            {
                checkExits();
            }
        }).start();


        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(checkField()) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (yet){
                        db.collection(Constant.ROOM).document(r.id).set(r);
                        finish();
                    }
                    else{
                        DocumentReference ref = db.collection(Constant.ROOM).document();
                        String idTmp = ref.getId();
                        r = new Room(
                                idTmp,
                                id_s,
                                edtxtName.getText().toString()
                        );
                        db.collection(Constant.ROOM).document(idTmp).set(r);
                        yet = true;
                        finish();
                    }
                }
            }
        });
    }

    private void checkExits(){

        db.collection(Constant.ROOM)
                .whereEqualTo("id", id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                yet = true;
                                r = document.toObject(Room.class);
                                Log.d(Constant.TAG, "Found Trip: " + r.id + " => " + r.name);
                                edtxtName.setText(r.name);
                                progressBar.setVisibility(View.GONE);
                            }
                        } else {
                            Log.d(Constant.TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        progressBar.setVisibility(View.GONE);

    }

    private boolean checkField(){
        if(edtxtName.getText().toString().isEmpty()){
            showMessageOK("Name is empty");
            return false;
        }
        return true;
    }

    private void showMessageOK(String message) {
        new AlertDialog.Builder(RoomActivity.this)
                .setMessage(message)
                .setNegativeButton("Ok", null)
                .create()
                .show();
    }
}
